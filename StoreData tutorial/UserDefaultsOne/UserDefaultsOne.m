//
//  UserDefaultsOne.m
//  StoreData tutorial
//
//  Created by Andrey Vasilev on 25.01.2018.
//  Copyright © 2018 Andrey Vasilev. All rights reserved.
//

#import "UserDefaultsOne.h"

@interface UserDefaultsOne ()

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UIButton *writeButton;
@property (nonatomic, strong) UIButton *readButton;

@end

@implementation UserDefaultsOne

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    float keyWindowHeight = [UIApplication sharedApplication].keyWindow.frame.size.width;
    
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 80, keyWindowHeight-40, 40)];
    [self.textField setBorderStyle:UITextBorderStyleLine];
    [self.view addSubview:self.textField];
    
    self.writeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.writeButton.frame = CGRectMake(20, 140,keyWindowHeight-40, 40);
    [self.writeButton setTitle:@"Сохранить" forState:UIControlStateNormal];
    [self.writeButton addTarget:self action:@selector(write) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.writeButton];
    
    self.readButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.readButton.frame = CGRectMake(20, 200,keyWindowHeight-40, 40);
    [self.readButton setTitle:@"Прочитать" forState:UIControlStateNormal];
    [self.readButton addTarget:self action:@selector(read) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.readButton];
    
    self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 260, keyWindowHeight-40, 40)];
    [self.view addSubview:self.textLabel];
}

- (void)write
{
    NSString *text = self.textField.text;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:text forKey:@"text"];
    [userDefaults synchronize];
}

- (void)read
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *text = [userDefaults objectForKey:@"text"];
    [self.textLabel setText:text];
}

@end
