//
//  main.m
//  StoreData tutorial
//
//  Created by Andrey Vasilev on 25.01.2018.
//  Copyright © 2018 Andrey Vasilev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
