//
//  PlistOneViewController.m
//  StoreData tutorial
//
//  Created by Andrey Vasilev on 25.01.2018.
//  Copyright © 2018 Andrey Vasilev. All rights reserved.
//

#import "PlistOneViewController.h"

@interface PlistOneViewController ()

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UIButton *writeButton;
@property (nonatomic, strong) UIButton *readButton;

@end

@implementation PlistOneViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    float keyWindowHeight = [UIApplication sharedApplication].keyWindow.frame.size.width;
    
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 80, keyWindowHeight-40, 40)];
    [self.textField setBorderStyle:UITextBorderStyleLine];
    [self.view addSubview:self.textField];
    
    self.writeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.writeButton.frame = CGRectMake(20, 140,keyWindowHeight-40, 40);
    [self.writeButton setTitle:@"Сохранить" forState:UIControlStateNormal];
    [self.writeButton addTarget:self action:@selector(write) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.writeButton];
    
    self.readButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.readButton.frame = CGRectMake(20, 200,keyWindowHeight-40, 40);
    [self.readButton setTitle:@"Прочитать" forState:UIControlStateNormal];
    [self.readButton addTarget:self action:@selector(advancedRead) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.readButton];
    
    self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 260, keyWindowHeight-40, 40)];
    [self.view addSubview:self.textLabel];
}

- (void)write
{
    NSString *text = self.textField.text;
    NSMutableDictionary *plistDictionary = [NSMutableDictionary new];
    [plistDictionary setObject:text forKey:@"text"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *filePath = [basePath stringByAppendingPathComponent:@"userData.plist"];
    
    [plistDictionary writeToFile:filePath atomically:YES];
}

- (void)read
{
    NSString *plistOneFilePath = [[NSBundle mainBundle] pathForResource:@"PlistOne" ofType:@"plist"];
    NSMutableDictionary *plistDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:plistOneFilePath];
    
    NSString *text = [plistDictionary objectForKey:@"text"];
    [self.textLabel setText:text];
}

- (void)advancedRead
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *userFilePath = [basePath stringByAppendingPathComponent:@"userData.plist"];
    NSMutableDictionary *userPlistDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:userFilePath];

    if (userPlistDictionary != nil) {
        NSString *text = [userPlistDictionary objectForKey:@"text"];
        [self.textLabel setText:text];
    } else {
        NSString *plistOneFilePath = [[NSBundle mainBundle] pathForResource:@"PlistOne" ofType:@"plist"];
        NSMutableDictionary *plistDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:plistOneFilePath];
        
        NSString *text = [plistDictionary objectForKey:@"text"];
        [self.textLabel setText:text];
    }
}

@end
